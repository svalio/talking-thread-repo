const validator = require("joi");
const schema = require("../../validators/UserValidator");

class UserValidate {
  async execute(user) {
    const result = await validator.validate(user, schema);
    if (result.error) {
      return false;
    }
    return true;
  }
}
module.exports = new UserValidate();
