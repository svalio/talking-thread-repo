const database = require("../../database");
const bcrypt = require("bcryptjs");
const cfg = require("../../database/hash/config");
const UserLoginService = require("./UserLoginService");

class CreateUser {
  async execute(body) {
    let result = false;
    let user = await database.User.findOne({
      where: {
        login: body.login,
      },
    });
    if (!user) {
      try {
        user = await database.User.create({
          name: body.name,
          login: body.login,
          password: await bcrypt.hash(body.password, cfg.saltRounds),
        });
        console.log("User created");
        const login = new UserLoginService();
        const tokens = await login.execute(body.login, body.password);
        console.log(tokens, "tokens");
        result = tokens;
      } catch (err) {
        console.log(err);
      }
    }
    return result;
  }
}
module.exports = CreateUser;
