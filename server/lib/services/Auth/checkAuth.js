const database = require("../../database");
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const passport = require("passport");
const Op = require("sequelize").Op;

const opts = {};
const cfg = require("../../routes/config");
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = cfg.secret;
opts.issuer = cfg.issuer;
opts.audience = cfg.audience;
passport.use(
  new JwtStrategy(opts, async (jwt_payload, done) => {
    const user = await database.User.findOne({
      where: { id: jwt_payload.id, refreshToken: { [Op.not]: null } },
    });
    if (!user) {
      done(null, false);
    } else {
      done(null, user);
    }
  })
);
module.exports = passport;
