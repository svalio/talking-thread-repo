const express = require("express");
const router = express.Router();
const Services = require("../services");
const userValidate = require("../services/validatorServices/UserValidatorService");
const roomValidate = require("../services/validatorServices/RoomValidatorService");
const passport = require("../services/Auth/checkAuth");

//RETURN ALL
router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const getRooms = new Services.RoomServices.GetRoomsService();
    const rooms = await getRooms.execute();
    if (rooms) {
      res.json(rooms);
    } else {
      res.sendStatus(404);
    }
  }
);

//FIND BY NAME
router.get(
  "/:name",
  async (req, res, next) => {
    const result = await roomValidate.execute({ name: req.params.name });
    if (result) {
      next();
    } else {
      res.sendStatus(400);
    }
  },
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const getRoom = new Services.RoomServices.GetRoomService();
    const room = await getRoom.execute(req.params.name);
    if (room) {
      res.json(room);
    } else {
      res.sendStatus(404);
    }
  }
);

//CREATE ROOM
router.post(
  "/",
  async (req, res, next) => {
    const result = await userValidate.execute(req.body.user);
    if (result) {
      next();
    } else {
      res.sendStatus(400);
    }
  },
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const createRoom = new Services.RoomServices.CreateRoomService(
      req.body.room.roomName
    );
    const room = await createRoom.execute(req.body.user); //req.body = user (creator of room)
    if (room) {
      res.status(200).send(room);
    } else {
      res.sendStatus(409);
    }
  }
);
//UPDATE ROOM
router.put(
  "/:id",
  async (req, res, next) => {
    const result = await userValidate.execute(req.body);
    if (result && Number.isInteger(Number(req.params.id))) {
      next();
    } else {
      res.sendStatus(400);
    }
  },
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const updateRoom = new Services.RoomServices.UpdateRoomService();
    const room = await updateRoom.execute(req.params.id, req.body);

    if (room) {
      res.json(room);
    } else {
      res.status(409);
    }
  }
);
//DELETE ROOM
router.delete(
  "/:id",
  async (req, res, next) => {
    if (Number.isInteger(Number(req.params.id))) {
      next();
    } else {
      res.sendStatus(400).end();
    }
  },
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const deleteRoom = new Services.RoomServices.DeleteRoomService();
    const room = await deleteRoom.execute(req.params.id);
    if (room) {
      res.sendStatus(200);
    } else {
      res.sendStatus(404);
    }
  }
);
//GET ALL USERS IN ROOM BY ROOMNAME
router.get(
  "/:name/users",
  async (req, res, next) => {
    const result = roomValidate.execute(req.params.name);
    if (result) {
      next();
    } else {
      res.sendStatus(400).end();
    }
  },
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const getUsersFromRoom = new Services.RoomServices.GetUsersFromRoomService();
    const users = await getUsersFromRoom.execute(req.params.name);
    if (users) {
      res.status(200).json(users);
    } else {
      res.sendStatus(404);
    }
  }
);
//CONNECT BY ID
router.post(
  "/:name/connect",
  async (req, res, next) => {
    const user = await userValidate.execute(req.body);
    if (user) {
      next();
    } else {
      res.sendStatus(400);
    }
  },
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const roomConnect = await new Services.RoomServices.RoomConnectService();
    const token = await roomConnect.execute(req.params.name, req.body);
    if (token) {
      res.status(200).send(token);
    } else {
      res.sendStatus(404);
    }
  }
);

//DISCONNECT BY ROOMNAME
router.post(
  "/:name/disconnect",
  async (req, res, next) => {
    const user = await userValidate.execute(req.body);
    if (user) {
      next();
    } else {
      res.sendStatus(400);
    }
  },
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const roomDisconnect = new Services.RoomServices.RoomDisconnectService();
    const result = await roomDisconnect.execute(req.params.name, req.body);
    if (result) {
      res.sendStatus(200);
    } else {
      res.sendStatus(404);
    }
  }
);

module.exports = router;
