const express = require("express");
const router = express.Router();
const Services = require("../services");
const userValidate = require("../services/validatorServices/UserValidatorService");
const passport = require("../services/Auth/checkAuth");
//LOGIN
router.post(
  "/login",
  async (req, res, next) => {
    const user = await userValidate.execute(req.body);
    if (user) {
      next();
    } else {
      res.sendStatus(400);
    }
  },
  async (req, res) => {
    const userLogin = new Services.UserServices.UserLoginService(req.session);
    const user = await userLogin.execute(req.body.login, req.body.password);
    if (user) {
      console.log("User '" + req.body.login + "' has logged in");
      res.status(200).send(user);
    } else {
      res.sendStatus(404);
    }
  }
);
//LOGOUT
router.post(
  "/logout/",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const userLogout = new Services.UserServices.UserLogoutService(req.session);

    const user = await userLogout.execute(req.user);
    if (user) {
      res.sendStatus(200);
    } else {
      res.sendStatus(404);
    }
  }
);
module.exports = router;
