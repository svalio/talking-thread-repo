const express = require("express");
const router = express.Router();
const Services = require("../services");
//create message
router.post("/:roomId/us=:userId", async (req, res) => {
  const createMessage = new Services.ChatServices.CreateMessageService();
  const result = await createMessage.execute(
    req.params.roomId,
    req.params.userId,
    req.body
  );
  if (result) {
    res.sendStatus(200);
  } else {
    res.sendStatus(400);
  }
});
//get all messages in the room by id
router.get("/:id", async (req, res) => {
  const getMessages = new Services.ChatServices.GetMessagesService();
  const result = await getMessages.execute(req.params.id);
  if (result) {
    res.json(result);
  } else {
    res.sendStatus(400);
  }
});
//update single message
router.put("/:roomId/us=:userId", async (req, res) => {
  const updateMessage = new Services.ChatServices.UpdateMessageService();
  const result = await updateMessage.execute(
    req.params.roomId,
    req.params.userId,
    req.body.message,
    req.body.updated
  );
  if (result) {
    res.sendStatus(400);
  } else {
    res.sendStatus(200);
  }
});
//delete single message by author and room
router.delete("/:roomId/us=:userId", async (req, res) => {
  const deleteMessage = new Services.ChatServices.DeleteMessageervice();
  const result = await deleteMessage.execute(
    req.params.roomId,
    req.params.userId,
    req.body.message
  );
  if (result) {
    res.sendStatus(200);
  } else {
    res.sendStatus(400);
  }
});

module.exports = router;
