module.exports = {
  development: {
    username: "postgres",
    password: "postgres",
    database: "talking-thread",
    host: "db",
    dialect: "postgres",
  },
  test: {
    username: "postgres",
    password: "postgres",
    database: "talking-thread",
    host: "db",
    dialect: "postgres",
  },
  production: {
    username: "postgres",
    password: "postgres",
    database: "talking-thread",
    host: process.env.DATABASE,
    dialect: "postgres",
  },
};
