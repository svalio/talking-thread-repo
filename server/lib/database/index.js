const Sequelize = require("sequelize");
const UserModel = require("./models/user");
const RoomModel = require("./models/room");
const MessageModel = require("./models/message");

const config = require("./config");

function createDatabase() {
  const environment = process.env.NODE_ENV || "development";
  const options = {
    operatorsAliases: false,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  };

  if (environment === "production") {
    return new Sequelize(process.env.DATABASE_URL, { ...options });
  }

  return new Sequelize({
    ...config[environment],
    ...options,
  });
}

const db = createDatabase();
const User = UserModel(db, Sequelize);
const Room = RoomModel(db, Sequelize);
const Message = MessageModel(db, Sequelize);
function connect() {
  db.authenticate()
    .then(() => {
      console.log("DATABASE CONNECTED.");
    })
    .catch(err => {
      console.error("ERROR CONNECTING TO DATABASE:", err);
    });
}

User.belongsTo(Room);
Room.hasMany(User);
Message.belongsTo(User);
Room.hasMany(Message, { onDelete: "CASCADE" });

db.sync()
  .then(() => {
    console.log("TABLES CREATED OR RELOADED");
  })
  .catch(err => {
    console.log(err);
  });

module.exports = { db, connect, User, Room, Message };
