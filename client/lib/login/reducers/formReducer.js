import {
  SIGN_IN_FORM_OPENED,
  SIGN_UP_FORM_OPENED,
  SIGN_IN_MODAL_OPENED,
  SIGN_IN_MODAL_CLOSED,
} from "../actionTypes";

const initialState = {
  isModalOpened: false,
  isSignInFormOpened: true,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SIGN_IN_FORM_OPENED:
      return { ...state, isSignInFormOpened: true };
    case SIGN_UP_FORM_OPENED:
      return { ...state, isSignInFormOpened: false };
    case SIGN_IN_MODAL_OPENED:
      return { ...state, isModalOpened: true };
    case SIGN_IN_MODAL_CLOSED:
      return { ...state, isModalOpened: false };
    default:
      return state;
  }
}
