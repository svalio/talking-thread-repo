import React from "react";
import Spinner from "@atlaskit/spinner";
import PropTypes from "prop-types";

export default class Init extends React.Component {
  componentDidMount() {
    const { onLoading, pathname, isUserLogOutInNextWindow } = this.props;
    onLoading(pathname);
    isUserLogOutInNextWindow();
  }

  render() {
    const { isLoading } = this.props;
    if (isLoading) {
      return (
        <div
          style={{
            position: "absolute",
            display: "flex",
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            height: "100%",
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Spinner size="xlarge" />
        </div>
      );
    }

    return this.props.children;
  }
}

Init.propTypes = {
  pathname: PropTypes.string.isRequired,
  onLoading: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isUserLogOutInNextWindow: PropTypes.func.isRequired,
  children: PropTypes.element.isRequired,
};
