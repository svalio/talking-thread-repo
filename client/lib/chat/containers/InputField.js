import { connect } from "react-redux";
import { push } from "connected-react-router";
import { addMessage } from "../actions";
import InputField from "../components/InputField";

export default connect(
  null,
  {
    push,
    addMessage,
  },
)(InputField);
