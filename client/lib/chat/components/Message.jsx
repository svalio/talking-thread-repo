import React from "react";
import PropTypes from "prop-types";
import marked from "marked";
import Avatar from "@atlaskit/avatar";

import Comment, { CommentAuthor } from "@atlaskit/comment";

const Message = props => {
  const msgContent = (
    <p
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{
        __html: marked(props.msgBody),
      }}
    />
  );
  return (
    <div className="msgHolder">
      <Comment
        avatar={
          <Avatar
            src={`https://robohash.org/${props.author}?set=set4`}
            size="medium"
          />
        }
        author={<CommentAuthor>{props.author}</CommentAuthor>}
        content={msgContent}
      />
    </div>
  );
};

Message.propTypes = {
  author: PropTypes.string,
  msgBody: PropTypes.string,
};

export default Message;
