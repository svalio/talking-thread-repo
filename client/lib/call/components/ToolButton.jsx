import React, { Component } from "react";
import PropTypes from "prop-types";
import Button from "@atlaskit/button";

class ToolButton extends Component {
  state = {
    isToggled: false,
  };

  static defaultProps = {
    // eslint-disable-next-line react/default-props-match-prop-types
    onClick: () => {},
  };

  toggle = () => {
    this.setState(
      ({ isToggled }) => ({
        isToggled: !isToggled,
      }),
      () => {
        this.props.onClick(this.state.isToggled);
      },
    );
  };

  render() {
    const isToggledIcon = this.state.isToggled
      ? this.props.untoggledIcon
      : this.props.toggledIcon;
    return (
      <div className="toolButton">
        <Button
          onClick={this.toggle}
          iconBefore={isToggledIcon}
          appearance="link"
        />
      </div>
    );
  }
}

export default ToolButton;

ToolButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  untoggledIcon: PropTypes.element.isRequired,
  toggledIcon: PropTypes.element.isRequired,
};
