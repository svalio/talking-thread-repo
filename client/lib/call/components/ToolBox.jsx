import React, { Component } from "react";
import PropTypes from "prop-types";
import CommentIcon from "@atlaskit/icon/glyph/comment";
import VidRaisedHandIcon from "@atlaskit/icon/glyph/vid-raised-hand";
import SettingsIcon from "@atlaskit/icon/glyph/settings";
import PeopleGroupIcon from "@atlaskit/icon/glyph/people-group";
import ToolButton from "./ToolButton";
import Section from "./Section";

class ToolBox extends Component {
  openChat = isToggled => {
    // eslint-disable-next-line no-unused-expressions
    isToggled ? this.props.onOpenSection("chat") : this.props.onOpenSection();
  };

  openCurrentUsers = isToggled => {
    // eslint-disable-next-line no-unused-expressions
    isToggled
      ? this.props.onOpenSection("currentUsers")
      : this.props.onOpenSection();
  };

  onRaiseHand = isToggled => {
    // eslint-disable-next-line no-unused-expressions
    isToggled ? this.props.raiseHand(isToggled) : this.props.raiseHand();
  };

  render() {
    const display = this.props.visible ? "toolbox" : "toolbox toolbox-hide";
    const className = this.props.section
      ? `${display} toolbox-radius`
      : display;

    return (
      <div>
        {this.props.section && <Section section={this.props.section} />}

        <div className={className}>
          <ToolButton
            onClick={this.openChat}
            toggledIcon={
              <CommentIcon
                primaryColor="rgba(255, 255, 255, 1);"
                size="large"
              />
            }
            untoggledIcon={
              <CommentIcon primaryColor="rgba(48, 197, 246, 1);" size="large" />
            }
          />
          <ToolButton
            toggledIcon={
              <VidRaisedHandIcon
                primaryColor="rgba(255, 255, 255, 1);"
                size="large"
              />
            }
            untoggledIcon={
              <VidRaisedHandIcon
                primaryColor="rgba(48, 197, 246, 1);"
                size="large"
              />
            }
            onClick={this.onRaiseHand}
          />
          <ToolButton
            toggledIcon={
              <SettingsIcon
                primaryColor="rgba(255, 255, 255, 1);"
                size="large"
              />
            }
            untoggledIcon={
              <SettingsIcon
                primaryColor="rgba(48, 197, 246, 1);"
                size="large"
              />
            }
          />
          <ToolButton
            onClick={this.openCurrentUsers}
            toggledIcon={
              <PeopleGroupIcon
                primaryColor="rgba(255, 255, 255, 1);"
                size="large"
              />
            }
            untoggledIcon={
              <PeopleGroupIcon
                primaryColor="rgba(48, 197, 246, 1);"
                size="large"
              />
            }
          />
        </div>
      </div>
    );
  }
}

export default ToolBox;

ToolBox.propTypes = {
  visible: PropTypes.bool.isRequired,
  onOpenSection: PropTypes.func.isRequired,
  raiseHand: PropTypes.func.isRequired,
  section: PropTypes.string,
};
