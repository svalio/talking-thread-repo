import React, { Component } from "react";
import PropTypes from "prop-types";
import CurrentUsers from "../containers/CurrentUsers";
import ChatPage from "../../chat";

export default class Section extends Component {
  sections = {
    chat: <ChatPage />,
    currentUsers: <CurrentUsers />,
  };

  render() {
    return this.sections[this.props.section];
  }
}

Section.propTypes = {
  section: PropTypes.string.isRequired,
};
