import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import VidAudioMutedIcon from "@atlaskit/icon/glyph/vid-audio-muted";
import VidCameraOffIcon from "@atlaskit/icon/glyph/vid-camera-off";
import SmallVideo from "./SmallVideo";

class VideoHolder extends Component {
  constructor(props) {
    super(props);

    this.video = React.createRef();
  }

  async componentDidMount() {
    this.props.joinSession();
  }

  componentDidUpdate() {
    if (this.video.current) {
      const { mediaStream } = this.getMainParticipant();

      this.video.current.srcObject = mediaStream;
    }
  }

  getMainParticipant() {
    const mainParticipant = this.props.participants.find(
      participant => participant.login === this.props.mainParticipant,
    );

    return mainParticipant;
  }

  renderParticipantVideoBlocks() {
    return this.props.participants.map(participant => (
      <SmallVideo
        isScreenSharingEnabled={this.props.isScreenSharingEnabled}
        key={participant.login}
        participant={participant}
        onClick={() => this.props.setMainParticipant(participant.login)}
      />
    ));
  }

  renderAvatar(mainParticipant) {
    const mainImgSrc = `https://robohash.org/${mainParticipant.login}?set=set4`;

    return (
      <div className="main-video-container__roboto">
        <img
          className="main-video-container__img"
          alt="Main video avatar"
          src={mainImgSrc}
        />
      </div>
    );
  }

  renderMainParticipant(mainParticipant) {
    const { isScreenSharingEnabled } = this.props;
    const classNames = [
      "main-video-container__video",
      mainParticipant.isRemote || isScreenSharingEnabled ? "remote" : null,
    ].filter(Boolean);

    // const videoSrc = mediaStream && URL.createObjectURL(mediaStream);

    return (
      <Fragment>
        <div className="main-video-container__name">
          {mainParticipant.login}
        </div>
        <div className="main-video-container__video-toolbar">
          {!mainParticipant.isVideoEnabled && (
            <VidCameraOffIcon
              className="main-video-container__icon"
              primaryColor="rgba(213, 4, 29, 0.8)"
              size="large"
            />
          )}
          {!mainParticipant.isAudioEnabled && (
            <VidAudioMutedIcon
              className="main-video-container__icon"
              primaryColor="rgba(213, 4, 29, 0.8)"
              size="large"
            />
          )}
        </div>

        <div className="main-video-container__participant">
          {/* {mainParticipant.mediaStream && mainParticipant.isVideoEnabled && (

          )} */}
          {mainParticipant.isVideoEnabled && (
            <video
              ref={this.video}
              id="remote-main-video"
              className={classNames.join(" ")}
              autoPlay
              muted
            />
          )}
        </div>
      </Fragment>
    );
  }

  render() {
    const mainParticipant = this.getMainParticipant();

    return (
      <div id="video-holder" className="main-container">
        <div id="main-video" className="main-video-container">
          {mainParticipant && this.renderAvatar(mainParticipant)}
          {mainParticipant && this.renderMainParticipant(mainParticipant)}
        </div>
        <div id="video-container" className="small-video-container">
          {this.renderParticipantVideoBlocks()}
        </div>
      </div>
    );
  }
}

export default VideoHolder;

VideoHolder.propTypes = {
  setMainParticipant: PropTypes.func.isRequired,
  joinSession: PropTypes.func.isRequired,
  isScreenSharingEnabled: PropTypes.bool.isRequired,
  mainParticipant: PropTypes.string.isRequired,
  participants: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      isRemote: PropTypes.bool,
      mediaStream: PropTypes.any,
      connectionId: PropTypes.string,
      isVideoEnabled: PropTypes.bool,
      isAudioEnabled: PropTypes.bool,
    }),
  ).isRequired,
};
