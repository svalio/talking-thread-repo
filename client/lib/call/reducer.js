import { combineReducers } from "redux";
import videoReducer from "./reducers/videoReducer";
import sectionReducer from "./reducers/sectionReducer";

export default combineReducers({
  videoReducer,
  sectionReducer,
});
