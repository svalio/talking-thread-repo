import { connect } from "react-redux";
import { joinSession, setMainParticipant } from "../actions";
import VideoHolder from "../components/VideoHolder";

const getMainParticipant = state => state.call.videoReducer.mainParticipant;
const getParticipants = state => state.call.videoReducer.participants;
const getScreenSharingEnabled = state => {
  const { videoReducer } = state.call;

  return videoReducer.isScreenSharingEnabled;
};

const mapStateToProps = state => ({
  mainParticipant: getMainParticipant(state),
  participants: getParticipants(state),
  isScreenSharingEnabled: getScreenSharingEnabled(state),
});

const mapDispatchToProps = dispatch => ({
  joinSession: () => dispatch(joinSession()),
  setMainParticipant: login => dispatch(setMainParticipant(login)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VideoHolder);
