import { SET_SESSION_NAME } from "./actionTypes";

const initialState = {
  sessionName: "",
};

export default function(state = initialState, action) {
  if (action.type === SET_SESSION_NAME) {
    return { ...state, sessionName: action.payload.sessionName };
  }
  return state;
}
