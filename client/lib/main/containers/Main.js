import { connect } from "react-redux";
import { push } from "connected-react-router";
import { logoutRequest } from "../../login/actions";
import { setSessionName } from "../actions";
import Main from "../components/Main";

const mapStateToProps = state => ({
  user: {
    login: state.login.userReducer.login,
    password: state.login.userReducer.password,
    publisher: state.login.userReducer.publisher,
  },
});

const mapDispatchToProps = dispatch => ({
  onLogout: () => dispatch(logoutRequest()),
  push: path => dispatch(push(path)),
  setSessionName: sessionName => {
    dispatch(setSessionName(sessionName));
    dispatch(push(`/${sessionName}`));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Main);
