import React, { Component } from "react";
import Textfield from "@atlaskit/textfield";
import ModalDialog, { ModalTransition } from "@atlaskit/modal-dialog";
import Form, { Field } from "@atlaskit/form";
import PropTypes from "prop-types";
import generateSessionName from "../generateSessionName";

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sessionName: generateSessionName(),
      intervalId: null,
    };
  }

  componentDidMount() {
    const intervalId = setInterval(() => {
      this.setState({
        sessionName: generateSessionName(),
        intervalId,
      });
    }, 10000);
  }

  componentWillUnmount() {
    const { state } = this;
    clearInterval(state.intervalId);
  }

  onSubmit = data => {
    if (data) {
      this.props.setSessionName(data);
    } else {
      this.props.setSessionName(this.state.sessionName);
    }
  };

  render() {
    const { user } = this.props;
    return (
      <div>
        <ModalTransition user={user}>
          <ModalDialog
            heading={`Hello, ${user.login}! Join a video session`}
            actions={[
              {
                text: "Go",
                form: "MainFormId",
                type: "submit",
              },
              {
                text: "Log Out",
                onClick: this.props.onLogout,
              },
            ]}
          >
            <Form onSubmit={data => this.onSubmit(data.Session)}>
              {({ formProps }) => (
                <form id="MainFormId" {...formProps}>
                  <p className="margin">
                    Go ahead, video chat with the whole team. In fact, invite
                    everyone you know. Talking Thread is a video conferencing
                    solution that you can use all day, every day, for free.
                  </p>
                  <Field label="Session name:" name="Session" defaultValue="">
                    {({ fieldProps }) => (
                      <Textfield
                        {...fieldProps}
                        placeholder={this.state.sessionName}
                      />
                    )}
                  </Field>
                </form>
              )}
            </Form>
          </ModalDialog>
        </ModalTransition>
      </div>
    );
  }
}

Main.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  onLogout: PropTypes.func.isRequired,
  setSessionName: PropTypes.func.isRequired,
};
