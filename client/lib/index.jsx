import React from "react";
import { render } from "react-dom";
import { Route, Switch, Redirect } from "react-router-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";

import "@atlaskit/css-reset";

import configureStore, { history } from "./configureStore";

import LandingPage from "./landing";
import LoginPage from "./login";
import MainPage from "./main";
import CallPage from "./call";
import Flag from "./shared/flag";

const store = configureStore();

const rootElement = document.getElementById("app");

const Root = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        <Route exact path="/" component={LandingPage} />
        <Route path="/login/" component={LoginPage} />
        <Route path="/main/" component={MainPage} />
        <Route path="/:call" component={CallPage} />
        <Redirect to="/" />
      </Switch>
    </ConnectedRouter>
    <Flag />
  </Provider>
);

render(<Root />, rootElement);
export default store;
